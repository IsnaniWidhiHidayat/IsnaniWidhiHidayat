<div align="center">
  <img src="https://i.ibb.co/2hCsVbR/unity-logo-banner.png" alt="Banner" style="max-width: 500%;">
</div>
<h1 align="center">Hi 👋, I'm Widhi</h1>

<p align="center">Dedicated Unity Developer with a passion for crafting immersive digital experiences. Over the years, I've honed my skills in game development, pushing the boundaries of creativity and technology.</p>

- 🧑 I’m currently working on **Premium Games**

- 🌱 I’m currently learning **Shaders**

- 🤝 I’m looking to collaborate on **Unity Projects**

- 🖥️ All of my projects are available at [Portofolio](https://isnanihidayat.wixsite.com/portofolio)

- 💬 Ask me about **Unity, C#, Game Programming, Game System**

- 📫 How to reach me [linkedin](https://www.linkedin.com/in/isnani-widhi-hidayat-b134b912a/), [Instagram](https://www.instagram.com/widhi_hidayat/)

- 🎵 Fun fact I like [One Ok Rock](https://www.instagram.com/oneokrockofficial)

#

<div align="center">
    <h5 align="center">🎮 Games</h5>
</div>

<ul>
    <li>
        <img align="center" height="30" src="https://i.ibb.co/Nmhw72k/appicon.png">
        <a href="https://linktr.ee/hellogoodboy" target="blank">Hello Goodboy</a>
    </li>
    <li>
        <img align="center" height="30" src="https://play-lh.googleusercontent.com/knoYK92j2tjjFLMe2qU2yBeR98f9g2ZuK6luqI9GUNKZqDyJUDBtjqvCgHGgKvRJuwQ=w240-h480-rw">
        <a href="https://play.google.com/store/apps/details?id=com.buyhousestudio.mineswipe&hl=en&gl=US" target="blank">Mine Swipe</a>
    </li>
    <li>
        <img align="center" height="30" src="https://play-lh.googleusercontent.com/7CN3IyzdKBkYZHOaPRpD1xNb6ml_M8lV5R9JydlVZLQkSfi_SSePaDAIL-m4oo05jw=w240-h480-rw">
        <a href="https://play.google.com/store/apps/details?id=com.buyhousestudio.randomaze" target="blank">Randomize</a>
    </li>
    <li>
        <img align="center" height="30" src="https://play-lh.googleusercontent.com/RR4SOEhAWr5bLRVitMTKwGCLyU8mVlFRrh0SfGG8U-aXXgSoIkiT4AaxEYrO1gPP9g=w240-h480-rw">
        <a href="https://play.google.com/store/apps/details?id=com.doolan.Kuartet" target="blank">Kuartet</a>
    </li>
    <li>
        <img align="center" height="30" src="https://play-lh.googleusercontent.com/2j2HIRHLZZ-qb-1uwvo9xJhe8vUXFoUX7CgaAG8f0n_t2W4M3UbiSIbo5rf7mJFCsg=w240-h480-rw">
        <a href="https://play.google.com/store/apps/details?id=com.doolan.Tapol" target="blank">Tapol</a>
    </li>
</ul>

#

<div align="center">
    <h5 align="center">🤝 Support</h5>
</div>

<ul>
    <li>
        <img align="center" height="30" src="https://i.ibb.co/Z6tmkwC/Helen.png">
        <a href="https://store.steampowered.com/app/2631350/Helen/" target="blank">Helen</a>
    </li>
    <li>
        <img align="center" height="30" src="https://cdn.cloudflare.steamstatic.com/steamcommunity/public/images/apps/1921340/74930669781ca5326d3b03a207729229daed7a4b.jpg">
        <a href="https://www.nintendo.com/us/store/products/fallen-legion-revenants-switch/" target="blank">Fallen Legion Revenants</a>
    </li>
</ul>

#

<div align="center">
    <h5 align="center">Socials</h5>
</div>

<div align="center">
  <a href="https://www.linkedin.com/in/isnani-widhi-hidayat-b134b912a/" target="_blank">
    <img src="https://raw.githubusercontent.com/maurodesouza/profile-readme-generator/master/src/assets/icons/social/linkedin/default.svg" width="47" height="30" alt="linkedin logo"  />
  </a>
  <a href="https://www.instagram.com/widhi_hidayat/" target="_blank">
    <img src="https://raw.githubusercontent.com/maurodesouza/profile-readme-generator/master/src/assets/icons/social/instagram/default.svg" width="47" height="30" alt="instagram logo"  />
  </a>
</div>

###

<div align="center">
    <h5 align="center">Tech & Tools</h5>
</div>

<div align="center">
  <img src="https://cdn-icons-png.flaticon.com/512/5969/5969346.png" height="30" alt="unity logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/csharp/csharp-original.svg" height="30" alt="csharp logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-original.svg" height="30" alt="cplusplus logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" height="30" alt="javascript logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg" height="30" alt="typescript logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg" height="30" alt="nodejs logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original.svg" height="30" alt="git logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/dot-net/dot-net-original.svg" height="30" alt="dot-net logo"  />
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original.svg" height="30" alt="vscode logo"  />
  <img src="https://us.v-cdn.net/6024696/uploads/GNV2GGYZ3ULE.png" height="30" alt="photon logo"  />
  <img src="https://phaser.io/images/img.png" height="30" alt="Phaser logo"  />
</div>
